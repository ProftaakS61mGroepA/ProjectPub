path="$(dirname "$(dirname "$(readlink -fm "$0")")")"

sudo docker stop projectpub-activemq

sudo docker network create projectpub_network
sudo docker-compose -f $path/deploy-activemq/docker-compose.yml up