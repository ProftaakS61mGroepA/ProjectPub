from time import sleep
from helper import send
from helper import errorhandler
from helper import setup_logger
from random import randint
import ConfigParser
import os
import json
import sys
import logging
import datetime
import time

GeneralSettings = None
OrderUrls = None
MinimumIntervalInSeconds = None
logger_general = "waiter_general_logger"
logger_performance = "waiter_performance_logger"

def Setup():
    print "starting setup"
    loggerGeneralPath = os.path.dirname(os.path.abspath(__file__))+"/log/"+logger_general+str(datetime.datetime.today().strftime("%b %d %Y %H:%M:%S"))+".log"
    loggerPerformancePath = os.path.dirname(os.path.abspath(__file__))+"/log/"+logger_performance+str(datetime.datetime.today().strftime("%b %d %Y %H:%M:%S"))+".log"

    setup_logger(logger_general, loggerGeneralPath,logging.INFO,"%(asctime)s %(message)s")
    setup_logger(logger_performance, loggerPerformancePath,logging.INFO,"%(asctime)s %(message)s")

    global GeneralSettings 
    GeneralSettings = ConfigParser.ConfigParser()
    GeneralSettings.read(os.path.dirname(os.path.abspath(__file__))+"/settings/general-settings.conf")
    
    global OrderUrls
    OrderUrls = ConfigParser.ConfigParser()
    OrderUrls.read(os.path.dirname(os.path.abspath(__file__))+"/settings/order-urls.conf")

    global MinimumIntervalInSeconds
    MinimumIntervalInSeconds = float(GeneralSettings.get("Waiter","MinimumIntervalInMiliseconds"))/1000.0;
    return

def Loop():
    for i in range(int(GeneralSettings.get("Waiter","Iterations"))):
        sleep(MinimumIntervalInSeconds)
        logging.getLogger(logger_general).info("Starting job")
        logging.getLogger(logger_performance).info("Starting job")

        startTime = time.clock()

        request = send(OrderUrls,"ProductsAll", None, logger_general, logger_performance)
        products = json.loads(request.text)

        if not len(products):
            errorhandler(logger_general, "No products found http code: " + str(request.status_code))
            continue

        request = send(OrderUrls,"OrderingCreate", None, logger_general, logger_performance)
        order = json.loads(request.text)

        if request.status_code is not 200:
            errorhandler(logger_general, "Could not create order")
            continue

        for i in range(0,len(order)*3):
            order["products"].append(products[randint(0,len(products)-1)])
            
        request = send(OrderUrls,"OrderingUpdate", order, logger_general, logger_performance)
        order = json.loads(request.text)
        if request.status_code is not 200:
            errorhandler(logger_general, "Could not update order")
            continue

        request = send(OrderUrls,"OrderingFulfill", order, logger_general, logger_performance)
        order = json.loads(request.text)
        if request.status_code is not 200:
            errorhandler(logger_general, "Could not fullfill order")
            continue

        request = send(OrderUrls,"OrderingTrydeliver", None, logger_general, logger_performance)
        if request.text:
            order = json.loads(request.text)
        if request.status_code is 204:
            logging.getLogger(logger_general).info("No deliverables")
        elif request.status_code is not 200:
            errorhandler(logger_general, "Could not get deliverables")
            continue
        elif request.status_code is 200:   
            request = send(OrderUrls,"OrderingFinishdeliver", order, logger_general, logger_performance)
            order = json.loads(request.text)
            if request.status_code is not 200:
                errorhandler(logger_general, "Could not deliver order")
                continue

        request = send(OrderUrls,"OrderingAlldelivered", None, logger_general, logger_performance)
        if request.text:
            order = json.loads(request.text)
        if request.status_code is 204:
            logging.getLogger(logger_general).info("No orders to be paid")
        elif request.status_code is not 200:
            errorhandler(logger_general, "Could not find orders to be paid")
            continue
        elif request.status_code is 200 and request.text != "[]":
            request = send(OrderUrls,"OrderingPay", order[0], logger_general, logger_performance)
            if request.status_code is not 204:
                errorhandler(logger_general, "Could not pay order")
    
        logging.getLogger(logger_performance).info("total_time: " + str(time.clock() - startTime) + " seconds")
    return

Setup()
Loop()
