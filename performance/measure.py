from time import sleep
import ConfigParser
import os
import requests
import json

GeneralSettings = None
OrderSettings = None
MinimumIntervalInSeconds = None

Orders = []

def Setup():
    print "starting setup"
    global GeneralSettings 
    GeneralSettings = ConfigParser.ConfigParser()
    GeneralSettings.read(os.path.dirname(os.path.abspath(__file__))+"/settings/general-settings.conf")
    
    global OrderSettings
    OrderSettings = ConfigParser.ConfigParser()
    OrderSettings.read(os.path.dirname(os.path.abspath(__file__))+"/settings/order-settings.conf")

    global MinimumIntervalInSeconds
    MinimumIntervalInSeconds = float(GeneralSettings.get("GeneralSettings","MinimumIntervalInMiliseconds"))/1000.0;
    return

def StressTest():
    print "starting stress testing"
    global Orders

    Orders.append(send(OrderSettings,"OrderingCreate", None))

    return

def send(setting,property,params):
    sleep(MinimumIntervalInSeconds)
    requestType = setting.get(property,"Method")
    if requestType=="GET":
        r = requests.get("http://" + setting.get("OrderUrl","Url") + ":" + setting.get("OrderUrl","PortNumber") + setting.get(property,"Url"))
    elif requestType=="POST":
        r = requests.post("http://" + setting.get("OrderUrl","Url") + ":" + setting.get("OrderUrl","PortNumber") + setting.get(property,"Url"),data=json.dumps(params))

    print r.url + " " + r.text

    return r.text

Setup()
StressTest()
