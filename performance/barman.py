from time import sleep
from helper import send
from helper import setup_logger
from helper import errorhandler
import ConfigParser
import os
import json
import logging
import datetime
import time

GeneralSettings = None
OrderUrls = None
MinimumIntervalInSeconds = None
logger_general = "barman_general_logger"
logger_performance = "barman_performance_logger"

def Setup():
    print "Starting setup"
    loggerGeneralPath = os.path.dirname(os.path.abspath(__file__))+"/log/"+logger_general+str(datetime.datetime.today().strftime("%b %d %Y %H:%M:%S"))+".log"
    loggerPerformancePath = os.path.dirname(os.path.abspath(__file__))+"/log/"+logger_performance+str(datetime.datetime.today().strftime("%b %d %Y %H:%M:%S"))+".log"

    setup_logger(logger_general, loggerGeneralPath,logging.INFO,"%(asctime)s %(message)s")
    setup_logger(logger_performance, loggerPerformancePath,logging.INFO,"%(asctime)s %(message)s")

    global GeneralSettings 
    GeneralSettings = ConfigParser.ConfigParser()
    GeneralSettings.read(os.path.dirname(os.path.abspath(__file__))+"/settings/general-settings.conf")
    
    global OrderUrls
    OrderUrls = ConfigParser.ConfigParser()
    OrderUrls.read(os.path.dirname(os.path.abspath(__file__))+"/settings/order-urls.conf")

    global MinimumIntervalInSeconds
    MinimumIntervalInSeconds = float(GeneralSettings.get("Barman","MinimumIntervalInMiliseconds"))/1000.0;
    return

def Loop():
    for i in range(int(GeneralSettings.get("Barman","Iterations"))):
        sleep(MinimumIntervalInSeconds)
        logging.getLogger(logger_general).info("Starting job")
        logging.getLogger(logger_performance).info("Starting job")

        startTime = time.clock()

        categories = send(OrderUrls,"ProductsCategories", None, logger_general, logger_performance).text
        if "FOOD" not in categories or "DRINK" not in categories or "OTHER" not in categories:
            errorhandler(logger_general, "Categories incomplete")
            continue

        request = send(OrderUrls,"OrderingCategorytrymake", ["DRINK"], logger_general, logger_performance)

        if request.status_code is not 200:
            logging.getLogger(logger_general).info("No makeable orders found")
            continue

        products = json.loads(request.text)

        for product in products["products"]:
            send(OrderUrls,"OrderingQueuecount", product, logger_general, logger_performance)

            if request.status_code is not 200:
                logging.getLogger(logger_general).info("Could not get queue count")
                continue

        request = send(OrderUrls,"OrderingFinishmake", request.text, logger_general, logger_performance)

        if request.status_code is 200:
            logging.getLogger(logger_general).info("Order processed")
        else:
            errorhandler(logger_general, "Could not process order")

        logging.getLogger(logger_performance).info("total_time: " + str(time.clock() - startTime) + " seconds")
    return

Setup()
Loop()
