from os import listdir
from os.path import isfile, join

barmanOverview = dict()
cookOververview = dict()
WaiterOverview = dict()
path = "log"

def Start():
    print "Started analyzing"

    files = [f for f in listdir(path) if isfile(join(path, f))]

    for file in files:
        if "barman_performance" in file:
            ExtractData(file,barmanOverview)
        elif "cook_performance" in file:
            ExtractData(file,cookOververview)
        elif "waiter_performance" in file:
            ExtractData(file,WaiterOverview)
    print "Done analyzing"
    return

def ExtractData(file, dict):
    with open(path+"/"+file) as lines:
        for line in lines:
            split = line.split()
            key = None
            value = None

            if len(split) is 5:
                key = split[2]
                value = split[3]
            if len(split) is 4:
                if split[2] == "total_time:":
                    key = split[2]
                    value = split[3]
            
            if key is not None and value is not None:
                if dict.has_key(key):
                    dict[key] = [float(value) +  dict[key][0],dict[key][1]+1]
                else:
                    dict[key] = [float(value),1]

    ProcesData(file, dict)
    return

def ProcesData(file, dict):
    print "---------"
    print "file: " + file
    for entry in dict:
        print entry + " took: " +  str(dict[entry][0] / dict[entry][1]) + " seconds with in total: " + str(dict[entry][1]) + " calls"
    return

Start()