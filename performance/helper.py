import requests
import json
import logging
import os
import time

class Request():
    def __init__(self, status_code, url, text):
        self.status_code = status_code
        self.url = url
        self.text = text
    

def setup_logger(logger_name, log_file, level, format):
    if not os.path.exists("log"):
        os.makedirs("log")

    os.mknod(log_file)

    l = logging.getLogger(logger_name)
    formatter = logging.Formatter(format)
    fileHandler = logging.FileHandler(log_file, mode='w')
    fileHandler.setFormatter(formatter)
    streamHandler = logging.StreamHandler()
    streamHandler.setFormatter(formatter)

    l.setLevel(level)
    l.addHandler(fileHandler)
    l.addHandler(streamHandler)    

def send(setting, property, params, logger, logger_performance):
    if isinstance(params,basestring):
        params = json.loads(params)

    shortStartTime = time.clock()

    request = None
    try:
        request = getattr(requests,setting.get(property,"Method").lower())("http://" + setting.get("OrderUrl","Url") + ":" + setting.get("OrderUrl","PortNumber") + setting.get(property,"Url"),json=params)

    except Exception as message:
        errorhandler(logger, str(message))
        request = Request(0,"","")
    
    logging.getLogger(logger_performance).info(setting.get(property,"Url")+": " + str(time.clock() - shortStartTime) + " seconds")

    if request.status_code < 200 or request.status_code > 299:
        errorhandler(logger, "Invalid statusCode "+str(request.status_code)+" on call " + setting.get(property,"Url"))

    logging.getLogger(logger).info(request.url + " " + request.text)

    return request

def errorhandler(logger,errorMessage):
    logging.getLogger(logger).warning("ERROR: "+errorMessage)