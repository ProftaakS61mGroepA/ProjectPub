# Setup
path="$(dirname "$(dirname "$(readlink -fm "$0")")")"
BRANCH="${BRANCH:-develop}"

mkdir -p $path/download
sudo rm -rf $path/download/stock-frontend-build.tar.gz
sudo rm -rf $path/download/stock-backend-build.tar.gz
sudo rm -rf $path/download/stock-frontend-build
sudo rm -rf $path/download/stock-backend-build

# Download
wget -O $path/download/stock-frontend-build.tar.gz https://gitlab.com/ProftaakS61mGroepA/ProjectPub_stock/-/jobs/artifacts/$BRANCH/download?job=gui-build
wget -O $path/download/stock-backend-build.tar.gz https://gitlab.com/ProftaakS61mGroepA/ProjectPub_stock/-/jobs/artifacts/$BRANCH/download?job=backend-build

# Unzip
unzip -j $path/download/stock-frontend-build.tar.gz -d $path/download/stock-frontend-build
unzip -j $path/download/stock-backend-build.tar.gz -d $path/download/stock-backend-build

# Set owner to student (not root)
chown -R student $path/download/stock-frontend-build
chown -R student $path/download/stock-backend-build

# Deploy
sudo docker stop application-frontend-stock
sudo docker stop application-backend-stock
sudo docker stop database-stock

sudo docker network create projectpub_network
sudo docker-compose -f $path/deploy-stock/docker-compose.yml build
sudo docker-compose  -f $path/deploy-stock/docker-compose.yml  up