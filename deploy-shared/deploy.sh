# Setup
path="$(dirname "$(dirname "$(readlink -fm "$0")")")"

# Deploy
sudo docker-compose -f $path/deploy-shared/docker-compose.yml down
sudo docker-compose -f $path/deploy-shared/docker-compose.yml up
