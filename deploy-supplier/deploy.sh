# Setup
path="$(dirname "$(dirname "$(readlink -fm "$0")")")"
BRANCH="${BRANCH:-develop}"

mkdir -p $path/download
sudo rm -rf $path/download/supplier-backend-build.tar.gz
sudo rm -rf $path/download/supplier-backend-build

# Download
wget -O $path/download/supplier-backend-build.tar.gz https://gitlab.com/ProftaakS61mGroepA/ProjectPub_Supplier/-/jobs/artifacts/$BRANCH/download?job=backend-build

# Unzip
unzip -j $path/download/supplier-backend-build.tar.gz -d $path/download/supplier-backend-build

# Set owner to student (not root)
chown -R student $path/download/supplier-backend-build

# Deploy
sudo docker stop application-backend-supplier

sudo docker network create projectpub_network
sudo docker-compose -f $path/deploy-supplier/docker-compose.yml up