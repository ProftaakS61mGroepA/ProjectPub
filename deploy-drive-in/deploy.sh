# Setup
path="$(dirname "$(dirname "$(readlink -fm "$0")")")"
BRANCH="${BRANCH:-develop}"

mkdir -p $path/download
sudo rm -rf $path/download/drive-in-frontend-build.tar.gz
sudo rm -rf $path/download/drive-in-backend-build.tar.gz
sudo rm -rf $path/download/drive-in-frontend-build
sudo rm -rf $path/download/drive-in-backend-build

# Download
wget -O $path/download/drive-in-frontend-build.tar.gz https://gitlab.com/ProftaakS61mGroepA/ProjectPub_drive-in/-/jobs/artifacts/$BRANCH/download?job=frontend-build
wget -O $path/download/drive-in-backend-build.tar.gz https://gitlab.com/ProftaakS61mGroepA/ProjectPub_drive-in/-/jobs/artifacts/$BRANCH/download?job=backend-build

# Unzip
unzip $path/download/drive-in-frontend-build.tar.gz -d $path/download/drive-in-frontend-build
unzip $path/download/drive-in-backend-build.tar.gz -d $path/download/drive-in-backend-build

# Set owner to student (not root)
chown -R student $path/download/drive-in-frontend-build
chown -R student $path/download/drive-in-backend-build

# Deploy
sudo docker stop application-fronted-drive-in
sudo docker stop application-backend-drive-in
sudo docker stop database-drive-in

sudo docker network create projectpub_network
docker-compose -f $path/deploy-drive-in/docker-compose.yml build
docker-compose  -f $path/deploy-drive-in/docker-compose.yml  up