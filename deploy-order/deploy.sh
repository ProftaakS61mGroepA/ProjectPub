# Setup
path="$(dirname "$(dirname "$(readlink -fm "$0")")")"
BRANCH="${BRANCH:-develop}"

mkdir -p $path/download
sudo rm -rf $path/download/order-frontend-build.tar.gz
sudo rm -rf $path/download/order-backend-build.tar.gz
sudo rm -rf $path/download/order-frontend-build
sudo rm -rf $path/download/order-backend-build

# Download
wget -O $path/download/order-frontend-build.tar.gz https://gitlab.com/ProftaakS61mGroepA/ProjectPub_ordering/-/jobs/artifacts/$BRANCH/download?job=gui-build
wget -O $path/download/order-backend-build.tar.gz https://gitlab.com/ProftaakS61mGroepA/ProjectPub_ordering/-/jobs/artifacts/$BRANCH/download?job=backend-build

# Unzip
unzip -j $path/download/order-frontend-build.tar.gz -d $path/download/order-frontend-build
unzip -j $path/download/order-backend-build.tar.gz -d $path/download/order-backend-build

# Set owner to student (not root)
chown -R student $path/download/order-frontend-build
chown -R student $path/download/order-backend-build

# Stop all 'ordering' containers
sudo docker ps -a | grep ordering | awk '{print $1}' | xargs sudo docker stop

# Deploy
sudo docker network create projectpub_network
sudo docker-compose -f $path/deploy-order/docker-compose.yml up

# Websocket notifications currently break on multiple instances
# Only manual scaling supported until this is fixed
# sudo docker-compose scale application-backend-ordering=3