#!/bin/sh -

cd "$(dirname "$0")"
git pull
export BRANCH="${BRANCH:-develop}"
./deploy-activemq/deploy.sh & ./deploy-supplier/deploy.sh & ./deploy-drive-in/deploy.sh & ./deploy-order/deploy.sh & ./deploy-stock/deploy.sh & ./deploy-shared/deploy.sh